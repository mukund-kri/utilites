from os import path

import git
from git import Repo


def is_git_repo(path):
    ''' Checks if a given path is a repository.

    Parameters:
    path (string): The path of the directory to check

    Returns:
    Repo: if repo exists it returns a gitlab.Repo object, else it returns None
    '''

    try:
        repo = Repo(path)
        return repo
    except git.exc.InvalidGitRepositoryError:
        return None
    except:
        return None


def clone_or_pull(remote_url, target_dir):
    ''' Clones a remote git repo if it does not exist on disk. If it exits
    then it pulls the latest

    Parameters:
    remote_url (string): The url of remote git repo
    target_dir (string): The local directory to which the repo will be cloned
    '''

    repo = is_git_repo(target_dir)

    if repo:
        repo.remotes.origin.pull()
        print("Successfully Pulled latest")
    else:
        repo = Repo.clone_from(remote_url, target_dir)
        print("Successfully Cloned repo")


def cache_update(repo_url, cache_dir):
    clone_or_pull(repo_url, cache_dir)
    
