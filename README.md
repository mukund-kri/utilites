# My Utilites. V2. #

A set of utilites written by me, to ease my development work.


### 1. Media Sort
Make managing my media files (movies/tv episodes/music) much easier.

### 2. Templatr
A very simple, single file from jinja2 template generating tool.

I wrote this because both cookiecutter and yeomam are project level generators,
and simple file level generators were hard to find.

#### today_i
I added another script to this util called today_i, which generates a jekyll
post in which I can write today's log.


### 3. day_of

A utility to generate boiler plate code when doing `X days of coding`
challenges. 

### 4. gitignore

Utility to generate .gitignore files for various languages / frameworks. As usual do ...

```shell
$ gitignore --help
```
to get usage.

### 5. Docker

(Mostly depricated. Docker compose makes this redundant)
A set of utils to manage my dockers over and above the commands
provided by docker itself and docker-compose.

## Development ##

### Nix. Setup

1. The nix shell command should take care setup in Nix enabled systems.

### Other Linux
	
2. Install the present package in development mode

```python
	> pip install -e .
```
	
3. ... Code ...

## Change Log ##

### 2.0 ###

2.0 is total rewrite. The sole aim is to make utils a python package for ease
of use across many computers.

### 3.0 (ongoing)

- Culling many utils that I don't need any more. Migrating to python 3.9+
features.
- Using PyInquirer when suited.
- Use pydantic for config file validation. 
- Unify config file handling. ie. one type of config file, validation framework
  etc.
- 

