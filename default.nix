with import <nixpkgs> {};
with pkgs.python37Packages;

stdenv.mkDerivation rec {
  name = "utils";
  env = buildEnv { name = name; paths = buildInputs; };
  SOURCE_DATE_EPOCH = 315532800;

  buildInputs = [
    libyaml
    
    python37
    python37Packages.virtualenvwrapper
  ];

  shellHook = ''
    export WORKON_HOME=$HOME/.virtualenvs
    source `which virtualenvwrapper.sh`
    workon utils
  '';
}
