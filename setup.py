from setuptools import setup


setup(
    name="utils",
    version="2.0.4",
    description="A set of scripts I use often",
    author="Mukund Krishnamurthy",
    author_email="mukund.kri@gmail.com",
    license="MIT",
    package_dir={
        "": "gitignore/src",
    },
    zip_safe=False,
    install_requires=[
        "requests==2.20.0",
        "click==8",
        "pyyaml==5.4.1",
        "jinja2",
        "cookiecutter==1.6.0",
        "git_python==1.0.3",
        "python-on-whales==0.50.0",
    ],
    scripts=[
        "mediasort/mediasort",
        "tmpltr/tmpltr",
        "day_of/day_of",
        # Generate a 'Today I Learnt blog post'
        "tmpltr/today_i",
        # Tool to generate .gitignore file
        "gitignore/scripts/gitignore",
        "docker_util/scripts/dplus",
    ],
)
