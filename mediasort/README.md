# Media Sort #

This script will eventually be a full fledged, AI backed media mager :)

Till then the only features supported ...

### TV

Organize tv episodes donwloaded from the web. This script also does a backup of
the media on to some kind of long term storage.

## Installation

This utility is installed with the others, when the main package is installed
then this utility will also be installed.

`mediasort` also need a configuration file called `config.yml` to be present in
the `~/.config/mediasort` folder. A sample of the same is provide in the current
folder.


## Usage

```
$> mediasort tv
```
