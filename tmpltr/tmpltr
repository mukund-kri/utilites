#!/usr/bin/env python3
# -*- mode: python -*-

__version__ = '0.0.1'


import os
import glob
from os.path import expanduser, join, basename

import click
from jinja2 import Template


@click.group()
def cli():
    pass


@cli.command(short_help="List templates")
def list():
    ''' List all jinja templates in the ~/.templatr folder '''

    home = expanduser('~/.templates')
    tpr = home +  '/*.jinja'
    for f in glob.glob(tpr):
        print(basename(f))


@cli.command(short_help='Generate code from template')
@click.argument('template')
@click.argument('target')
@click.option('--vars', '-v', nargs=2, type=click.Tuple([str, str]), multiple=True) 
def gen(template, target, vars):
    '''
    - read the template file into a string
    - add the variable from the command line into the template context
    - render and write output to specified file
    '''
    vars = {v[0]: v[1] for v in vars}
    
    tpl_home = expanduser('~/.templates')
    template = join(tpl_home, '{}.jinja'.format(template))
    with open(template) as f:
        txt = f.read()

    with open(target, 'w') as f:
        template = Template(txt)
        result = template.render(**vars)
        f.write(result)


if __name__ == '__main__':
    cli()
    
